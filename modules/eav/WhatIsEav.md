# What is EAV?

## Definition
As [wiki says](https://en.wikipedia.org/wiki/Entity%E2%80%93attribute%E2%80%93value_model), **EAV** _(Entity Attribute Value)_ - is a _data model_ 
to encode, in space-efficient manner, _entities_ where number of _attributes_ that can be used to describe them is 
potentially vast, but the number that will actually apply to a given entity is relatively modest.

To be simple, EAV - is _data model_ to store entities with different nature.

EAV data model also called:
- OTLT – One True Lookup Table
- Open Schema
- Diabolically Enticing Method Of Data Storage (DEMONS)


## Example
To implement basic EAV we need 3 tables:

**entities** 

| entity_id | entity_name  |
|:---------:|:------------:|
| 1         | Apple        |
| 2         | Wine         |
| 3         | Table        |

**attributes**

| attribute_id | attribute_name  |
|:------------:|:---------------:|
| 1            | Weight          |
| 2            | Taste           |
| 3            | Wood type       |
| 4            | Volume          |
| 5            | Wine type       |

**data**

| data_id   | entity_id    | attribute_id | value   |
|:---------:|:------------:|:------------:|:-------:|
| 1         | 1            |  1           | 200 g.  |
| 2         | 1            |  2           | Juicy   |
| 3         | 2            |  5           | Red     |
| 4         | 2            |  4           | 500 ml  |
| 5         | 3            |  1           | 8 kg    |
| 6         | 3            |  3           | Oak     |
| 7         | 2            |  2           | tasty   |

DDL for this data scheme:

```sql
CREATE TABLE `entities` (
  `entity_id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_name` varchar(30) NOT NULL,
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `entities_entity_name_uindex` (`entity_name`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `attributes` (
  `attribute_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_name` varchar(30) NOT NULL,
  PRIMARY KEY (`attribute_id`),
  UNIQUE KEY `attributes_attribute_name_uindex` (`attribute_name`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `data` (
  `data_id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`data_id`),
  UNIQUE KEY `data_ukey` (`entity_id`,`attribute_id`),
  KEY `data_fk_attrib` (`attribute_id`),
  CONSTRAINT `data_fk_attrib` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`attribute_id`),
  CONSTRAINT `data_fk_entities` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`entity_id`) 
  ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

```

As you can see EAV is harder to start then simple flat table but it's very flexible and easier to maintain 
(_notice we used only 3 entities, real life example will be more dramatic_):

| id  |  name  |  weight  | taste  |  wood_type |  volume  | wine_type |
|:---:|:------:|:--------:|:------:|:----------:|:--------:|:---------:|
| 1   | Apple  | 200      | Juicy  | _NULL_     | _NULL_   | _NULL_    |
| 2   | Wine   | _NULL_   | tasty  | _NULL_     | 500 ml   | Red       |
| 3   | Table  | 8 kg     | _NULL_ | Oak        | _NULL_   | _NULL_    |



## Advantages
- adding attributes without schema change
- number of attributes can be more than allowable number of columns (MySql - 
[4096 columns per table](http://dev.mysql.com/doc/refman/5.7/en/column-count-limit.html "Mysql Documentation"))
- it stores data in space efficient way(as in example, there is no need to put _NULLs_ where is no sense for the 
attribute)

## Disadvantages

### Anti-pattern

But as I searched info about EAV on the Internet I noticed that it's often called anti-pattern. 
And I tried to figure out what's wrong with EAV.<br>
Searched again and again I figured out, that main reason why it's called anti-pattern is bad design. 

For example  this [Bill Karwin's post](http://karwin.blogspot.com/2009/05/eav-fail.html) from 2009 tells that EAV fails 
because it using varchar as value type in **data** table.  

![logo](img/eav_fails.jpg "EAV fails")

If we stop to develop an idea of classic EAV example, it's the right statement. But we are designers, so we 
can just add **_type_** field to attributes table ,drop our **data** table and add tables like **data_varchar**, **data_int**,
**data_float**... If you want flexibility, you should to think how to do it. Next time we need to get SUM  we will obtain 
it without using CONVERT SQL function. (this is approximately how it works in Magento)

### Some from me

In previous chapters I told EAV is really cool thing. Very flexible and space efficient. But If you use EAV you'll 
obtain narrow but very long table.

## To EAV or not to EAV?

EAV is great but as all in this world should be used only in cases were it will be useful. For example: If you have 
little bakery, there is no need to use EAV. If your summary number of all possible attributes is less then 
approximately 30-50 there is also no need in EAV.
But if number of attributes will grow rapidly , and all entities in your store is of different nature, use EAV. 

Using EAV for all kinds of entities in your data model is also bad idea. Remember, always wins the hybrid decisions.

## Where to next?
In this short article were described basic EAV data model. Next step we're going to describe how EAV is implemented in 
**Magento**.
