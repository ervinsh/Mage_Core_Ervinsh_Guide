# Going deep in checkout

## 1. Intro
In this part I want to describe the goals of this article. 
I want to describe checkout process on Magento, the tools 
which I will use in this process is browser, debugger and
clean Magento installation with one simple product (I decided to 
use only one product to describe the process in the most 
simple way, without some complication for now).

## 2. Let's start
I already created product and made it sealable, so there is 
no reasons not click pretty blue button to add my perfect 
test product to cart

![logo](img/add-to-cart.png "Add to cart")

This action on standard magento installation will lead us to 
checkout/cart page, where we can proceed to checkout or return
and buy more products. As there is the only one product in the 
whole shop, and it so perfect that we don't need more, we will 
proceed to checkout.

After this we will be forwarded to checkout/onepage where we can 
chose to checkout as guest or as registered user.

## 3. Beginning of interesting things

After filling all the fields in Billing information, click continue.

![logo](img/billing.png)

This will cause to save the billing address. After this action in 
database table sales_flat_quote_address was created two rows. 
First stands for billing and the second stands for shipping addresses.

Let's look on the code of this controller action:
```php
    public function saveBillingAction()
    {
        if ($this->_expireAjax()) {
            return;
        }
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost('billing', array());
            $customerAddressId = $this->getRequest()->getPost('billing_address_id', false);

            if (isset($data['email'])) {
                $data['email'] = trim($data['email']);
            }
            $result = $this->getOnepage()->saveBilling($data, $customerAddressId);

            if (!isset($result['error'])) {
                if ($this->getOnepage()->getQuote()->isVirtual()) {
                    $result['goto_section'] = 'payment';
                    $result['update_section'] = array(
                        'name' => 'payment-method',
                        'html' => $this->_getPaymentMethodsHtml()
                    );
                } elseif (isset($data['use_for_shipping']) && $data['use_for_shipping'] == 1) {
                    $result['goto_section'] = 'shipping_method';
                    $result['update_section'] = array(
                        'name' => 'shipping-method',
                        'html' => $this->_getShippingMethodsHtml()
                    );

                    $result['allow_sections'] = array('shipping');
                    $result['duplicateBillingInfo'] = 'true';
                } else {
                    $result['goto_section'] = 'shipping';
                }
            }

            $this->_prepareDataJSON($result);
        }
    }

```

With xDebug we obtained the request params of saveBillingAction:

![logo](img/billing-request-params.png)

This params are passed as params to the most interesting from these lines:
```
 $result = $this->getOnepage()->saveBilling($data, $customerAddressId);
```
This code just calls the singleton instance of Mage_Checkout_Model_Onepage saveBillingMethod.
This method makes some checks and pre-work with address data. After
all routine in this method done called method:
```
$this->getQuote()->collectTotals();
```
This method takes the quote from checkout/session, and calls it
method collectTotals, where the most interesting thing in all checkout process
happening.

So let's go more deep in this process.

# 4. Deep checkout
First of all Magento checks if totals already collected, if so, then no
need to recollect them, as this process is quite expensive.
Then Magento devs, gives us (undermagento devs) opportunity to customize
collecting process with observer
```
 Mage::dispatchEvent($this->_eventPrefix . '_collect_totals_before', array($this->_eventObject => $this));
```
In this case event_prefix is sales_quote. So you can create your own observer method, 
which will intercept this event and can do something with quote, before totals
collected.

After some start subtotals field  setting there comes foreach loop through the
all of addresses of this quote(classic case when there are only two shipping and billing
but you can implement multi-shipping or multi-billing, magento haven't this
features, but not deny to do so).

```
    foreach ($this->getAllAddresses() as $address) {
            $address->setSubtotal(0);
            $address->setBaseSubtotal(0);

            $address->setGrandTotal(0);
            $address->setBaseGrandTotal(0);

            $address->collectTotals();

            $this->setSubtotal((float) $this->getSubtotal() + $address->getSubtotal());
            $this->setBaseSubtotal((float) $this->getBaseSubtotal() + $address->getBaseSubtotal());

            $this->setSubtotalWithDiscount(
                (float) $this->getSubtotalWithDiscount() + $address->getSubtotalWithDiscount()
            );
            $this->setBaseSubtotalWithDiscount(
                (float) $this->getBaseSubtotalWithDiscount() + $address->getBaseSubtotalWithDiscount()
            );

            $this->setGrandTotal((float) $this->getGrandTotal() + $address->getGrandTotal());
            $this->setBaseGrandTotal((float) $this->getBaseGrandTotal() + $address->getBaseGrandTotal());
        }
```

In the first line of foreach there init process for address subtotal fields.
And then called one more collectTotals method, but there are difference, this
collectTotals is Address method.

Let's look what this method looks, like. It's pretty short for magento:
```
    public function collectTotals()
    {
        Mage::dispatchEvent($this->_eventPrefix . '_collect_totals_before', array($this->_eventObject => $this));
        foreach ($this->getTotalCollector()->getCollectors() as $model) {
            $model->collect($this);
        }
        Mage::dispatchEvent($this->_eventPrefix . '_collect_totals_after', array($this->_eventObject => $this));
        return $this;
    }
```
And as you can see also gives us opportunity for customization of collect totals process 
for address.
To customize just use observer event sales_quote_address_collect_totals_before(after).
Method loks very simple, but this was error to think it easier to understand.
You should understand that under clear magento installation 
$this->getTotalCollector()->getCollectors() returns 11 elements:

![logo](img/address-collect-totals.png)


